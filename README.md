# Usage #


    import pyloaddata as pld
    datafiles = ('data/ResFinder_20200125.catmapstat.gz',
             'data/Silva_20200116.catmapstat.gz',
             'data/ResFinder_20200125.refdata.gz',
             'data/Silva_20200116.refdata.gz')
    data = pld.LoadData(*datafiles)

Please not that paths to certain data files are hard coded. 