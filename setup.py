#!/usr/bin/env python
''' Setup file for pyloaddata
'''

from distutils.core import setup

setup(name='pyLoaddata',
      version='0.1',
      description='Load KMA mapped data',
      author='Christian Brinch',
      author_email='cbri@food.dtu.dk',
      packages=['pyloaddata'],
      install_requires=['pandas>=1.2.4'],
      )
