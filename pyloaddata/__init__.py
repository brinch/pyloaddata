'''
    pyLoadData init script
'''

__title__ = "pyloaddata"
__author__ = "Christian Brinch"
__email__ = "cbri@food.dtu.dk"
__copyright__ = "Copyright 2021 C. Brinch"
__version__ = 0.5
__all__ = ['load_data']

from .load_data import *

