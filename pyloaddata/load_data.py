# -*- coding: utf-8 -*-
''' Data preparation script for KMA mapped data.
    Written by Christian Brinch <cbri@dtu.dk>, Copyright 2021
'''

import pandas as pd

PATH = '/Users/cbri/ScienceData/Projects/globalsewage/'
CLUSTER = PATH+'resfinder_homology_reduce/ResFincer2Geneclust.UC90.txt'
RESCLASS = PATH+'metadata/gene_metadata/ResFinder.class'
RESTYPE = PATH+'metadata/gene_metadata/ResFinder.phenotype'


def select(self, tax_level):
    ''' Monkey patch pandas with this shorthand '''
    return self.groupby(axis=1, level=tax_level).sum()


class LoadData(dict):
    ''' Load and sanitize data and metadata

    '''

    def __init__(self, *datafiles):
        dict.__init__(self)

        pd.DataFrame.select = select

        # Load metadata
        _ = [self._get_metadata(file) for file in datafiles if 'meta' in file.lower()]

        # Load ResFinder and Silva
        for database in ['ResFinder', 'Silva', 'genomic']:
            datafile = [file for file in datafiles if database in file
                        and 'catmapstat' in file]
            refdata = [file for file in datafiles if database in file
                       and 'refdata' in file]

            if datafile and refdata:
                self._get_data(database, datafile[0], refdata[0])
                if database == 'ResFinder':
                    self._homology_reduction(CLUSTER)
                    self._amalgamate(RESCLASS, 'amr_class')
                    self._amalgamate(RESTYPE, 'amr_phenotype')

        if 'metadata' in self.keys():
            # Merge technical replicates in all data sets
            self._merge_tech_repl()

    def _merge_tech_repl(self):
        """ Merge technical replicates

        Samples with identical sample names will be merged.

        """
        for key in self.keys():
            if key != 'metadata' and 'Sample name' in self['metadata'].columns:
                self[key] = self[key].groupby(self['metadata']['Sample name']).sum()

    def _get_metadata(self, file):
        """ Read the metadata file

            Sanitize the metadata by removing unused columns and cleaning it up

        """
        self['metadata'] = pd.read_csv(file,
                                       sep=';',
                                       encoding="ISO-8859-1",
                                       index_col=0,
                                       skiprows=6)

    def _get_data(self, database, file, reffile):
        """ Read in the actual data files

            Drop unused entries, removed unused columns, remove the clutter from the
            sample names, correct for reference length, and unfold into a matrix.

        """
        data = pd.read_csv(file,
                           sep='\t',
                           encoding="ISO-8859-1",
                           index_col='sample')
        refdata = pd.read_csv(reffile,
                              sep='\t',
                              encoding="ISO-8859-1",
                              skiprows=1,
                              index_col='# id')

        # Remove unused columns to preserve memory
        data['id'] = [i.split(';')[0].split(' ')[0] for i in data['refSequence']]
        data = data[['id', 'fragmentCountAln']]

        # Fix sample names
        data.index = data.index.str.replace(r'.*__', r'', regex=True)
        data.index = ["_".join(i.split('_')[:-4]) for i in data.index]

        # Correct for reference length
        data = pd.merge(data, refdata['id_len'],
                        left_on='id', right_index=True)
        data['fragmentCountAln'] = data['fragmentCountAln'] / \
            (data['id_len'] / 1000)
        data.drop('id_len', axis=1, inplace=True)

        refdata = refdata.fillna('Unknown')

        # Make multiindex table from taxonomy
        if database in ['Silva', 'genomic']:
            taxlevels = ['superkingdom_name', 'kingdom_name', 'phylum_name', 'class_name',
                         'order_name', 'family_name', 'genus_name', 'species_name', 'strain_name']
            data = pd.merge(data, refdata[taxlevels], left_on='id', right_index=True)
            data = data.pivot_table(index=data.index, columns=taxlevels+['id'],
                                    aggfunc='sum', values='fragmentCountAln', fill_value=0.)
        else:
            data = data.pivot_table(index=data.index, columns='id',
                                    aggfunc='sum', values='fragmentCountAln', fill_value=0.)

        self[database] = data

    def _homology_reduction(self, reffile):
        """ Homology reduction

        Reduce the resfinder genes based on 90% identity clusters

        """
        refdata = pd.read_csv(reffile,
                              sep='\t',
                              encoding="ISO-8859-1")
        self['uc90'] = pd.merge(self['ResFinder'].T, refdata[['query', 'target']],
                                left_index=True, right_on='query').groupby('target').sum().T

    def _amalgamate(self, reffile, key):
        """ Amalgamate data on phenotypic classes

        """
        refdata = pd.read_csv(reffile,
                              sep='\t',
                              encoding="ISO-8859-1",
                              index_col='# Gene')

        refdata = refdata.loc[self['ResFinder'].columns]
        self[key] = self['ResFinder'].dot(refdata)
